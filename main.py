from telegram.ext import Updater
from lib.env_var_selector import get_env_var
from app.config.routes import HANDLERS


def main() -> None:



    updater = Updater(get_env_var('BOT_TOKEN'), workers=100)

    for handler in HANDLERS:
        updater.dispatcher.add_handler(handler)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
