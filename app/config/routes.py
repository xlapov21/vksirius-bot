from telegram.ext import CommandHandler, MessageHandler, Filters, CallbackContext, CallbackQueryHandler
from telegram import Update

from app.controllers.actions.choice_faq import choice_faq
from app.controllers.actions.waiting import waiting
from app.controllers.actions.write_to_operator import write_to_operator
from app.controllers.commands.change_operator import change_operator_command
from app.controllers.commands.faq import faq_command
from app.controllers.commands.help import help_command
from app.controllers.commands.request_operator import request_operator
from app.controllers.commands.set_language import set_language_command, ru, en
from app.controllers.commands.start import start_command
from app.models.user import User

from app.services.logging import log
from lib.message_selector import get_text

ACTIONS = {
    'waiting': waiting,
    'write_to_operator': write_to_operator,
    'choice_faq': choice_faq
}


def resend_to_action(update: Update, context: CallbackContext) -> None:
    log(update)
    message = update.message
    user = User.find_by_chat_id(message.chat.id)
    action = ACTIONS[user.current_action]
    if action:
        action(message, user)
    else:
        message.reply_text(get_text('message_bundle.general.no_action', user.language))


def reply_to_no_text(update: Update, context: CallbackContext) -> None:
    log(update)
    update.message.reply_text(get_text('message_bundle.general.no_text', 'ru'))


HANDLERS = [
    CommandHandler('start', start_command),
    CommandHandler('set_language', set_language_command),
    CommandHandler('request_operator', request_operator),
    CommandHandler('FAQ', faq_command),
    CommandHandler('change_operator', change_operator_command),
    CommandHandler('help', help_command),

    CommandHandler('language', set_language_command),

    MessageHandler(
        Filters.text |
        Filters.caption |
        Filters.caption_entity, resend_to_action),
    MessageHandler(
        (not Filters.text) |
        (not Filters.caption) |
        (not Filters.caption_entity), reply_to_no_text),


    CallbackQueryHandler(ru, pattern='ru'),
    CallbackQueryHandler(en, pattern='en'),
]

