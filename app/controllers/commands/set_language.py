from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup, Bot, Message
from telegram.ext import CallbackContext, CommandHandler, CallbackQueryHandler

from app.controllers.commands.start import start_command
from app.models.user import User
from app.services.logging import log, get_message_text
from lib.db_connector import DBConnector
from lib.env_var_selector import get_env_var
from lib.message_selector import get_text


def set_language_command(update: Update, context: CallbackContext) -> None:
    log(update)
    buttons = []
    results = DBConnector.execute("""SELECT * FROM languages""")
    for language in results:
        buttons.append(InlineKeyboardButton(text=f'{language[2]}', callback_data=f'{language[1]}'))

    update.message.reply_text(
        get_text("message_bundle.command.set_language.answer", User.find_by_chat_id(update.message.chat.id).language),
        reply_markup=InlineKeyboardMarkup([buttons]))


def ru(update: Update, context: CallbackContext):
    chat_id = update.callback_query.message.chat.id
    user = User.find_by_chat_id(chat_id)
    user.language = 'ru'
    send_result(user)
    user.current_action = 'waiting'
    user.save()


def en(update: Update, context: CallbackContext):
    chat_id = update.callback_query.message.chat.id
    user = User.find_by_chat_id(chat_id)
    user.language = 'en'
    send_result(user)
    user.current_action = 'waiting'
    user.save()


def send_result(user: User):
    Bot(get_env_var('BOT_TOKEN')).send_message(
        user.chat_id,
        get_text('message_bundle.command.set_language.result',
                 user.language)
    )
