from telegram import Update
from telegram.ext import CallbackContext

from app.helpers.help_list_generator import generate_text_list
from app.models.user import User
from app.services.logging import log
from lib.message_selector import get_text


def help_command(update: Update, context: CallbackContext) -> None:
    log(update)
    user = User.find_by_chat_id(update.message.chat.id)
    update.message.reply_text(get_text('message_bundle.command.help.answer', user.language) % (
        user.current_action,
        generate_text_list(user.language)
    ))
