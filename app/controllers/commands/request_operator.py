from telegram import Update
from telegram.ext import CallbackContext

from app.models.user import User
from app.services.logging import log
from lib.message_selector import get_text


def request_operator(update: Update, context: CallbackContext) -> None:
    log(update)
    user = User.find_by_chat_id(update.message.chat.id)
    user.current_action = 'write_to_operator'
    user.save()
    update.message.reply_text(get_text('message_bundle.command.request_operator.answer', user.language))
