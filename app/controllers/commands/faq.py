from telegram import Update
from telegram.ext import CallbackContext

from app.helpers.faq_list_generator import generate_text_list
from app.models.user import User
from app.services.logging import log
from lib.message_selector import get_text


def faq_command(update: Update, context: CallbackContext) -> None:
    log(update)
    user = User.find_by_chat_id(update.message.chat.id)
    user.current_action = 'choice_faq'
    user.save()
    update.message.reply_text(
        get_text('message_bundle.command.FAQ.answer', user.language) %
        generate_text_list(user.language))
