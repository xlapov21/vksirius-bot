from telegram import Update
from telegram.ext import CallbackContext

from app.models.user import User
from app.services.logging import log
from app.services.operator import get_random_operator
from lib.message_selector import get_text


def change_operator_command(update: Update, context: CallbackContext) -> None:
    log(update)
    user = User.find_by_chat_id(update.message.chat.id)
    user.current_operator_id = get_random_operator()
    user.current_action = 'waiting'
    user.save()
    update.message.reply_text(get_text('message_bundle.command.change_operator.answer', user.language))
