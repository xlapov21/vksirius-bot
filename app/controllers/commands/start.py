from telegram import Update
from telegram.ext import CallbackContext

from app.models.user import User
from app.services.logging import log
from app.services.operator import get_random_operator
from lib.message_selector import get_text


def start_command(update: Update, context: CallbackContext) -> None:
    log(update)
    message = update.message
    user = User.find_by_chat_id(message.chat.id)
    answer_code = 'message_bundle.command.start.answer.'
    if user:
        answer_code += 'repetition'
        user.current_action = 'waiting'
        user.save()
    else:
        answer_code += 'first'
        user = User.create(
            message.chat.id,
            message.from_user.username,
            message.from_user.last_name,
            message.from_user.first_name,
            get_random_operator()
        )

    update.message.reply_text(get_text(answer_code, user.language))
