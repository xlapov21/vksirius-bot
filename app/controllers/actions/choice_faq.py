from telegram import Message

from app.models.faq import FAQ
from app.models.user import User
from app.services.logging import get_message_text
from lib.message_selector import get_text


def choice_faq(message: Message, user: User):
    question_id = get_message_text(message)
    if question_id.isdigit():
        question = FAQ.find_by_id(question_id)
        if question:
            if user.language == 'ru':
                message_text = question.answer_ru
            elif user.language == 'en':
                message_text = question.answer_en
            else:
                message_text = 'message_bundle.general.no_action'
            user.current_action = 'waiting'
            user.save()
        else:
            message_text = get_text('message_bundle.action.choice_faq.uncorrected', user.language)
    else:
        message_text = get_text('message_bundle.action.choice_faq.uncorrected', user.language)
    message.reply_text(message_text)
