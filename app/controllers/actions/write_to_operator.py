from telegram import Message, Bot

from app.models.user import User
from app.services.logging import get_message_text
from lib.env_var_selector import get_env_var
from lib.message_selector import get_text


def write_to_operator(message: Message, user: User):
    Bot(get_env_var('BOT_TOKEN')).send_message(
        user.current_operator_id,
        get_text('message_bundle.action.write_to_operator.message_for_operator', 'ru')
        % (
            f'ChatID: {user.chat_id}, {user.last_name} {user.first_name}',
            get_message_text(message)
        )
    )
    message.reply_text(get_text(
        'message_bundle.action.write_to_operator.corrected',
        user.language
    ))
    user.current_action = 'waiting'
    user.save()
