import telegram
from telegram import Message
import re

from app.models.user import User
from app.services.sentence_determinant import SentenceDeterminant
from app.services.logging import get_message_text
from app.services.operator import is_operator
from lib.env_var_selector import get_env_var
from lib.message_selector import get_text

OPERATOR_ANSWER_REGEX = r'^(\d+)\s+(.+)$'

SENTENCE_DETERMINANT = SentenceDeterminant()


def waiting(message: Message, user: User):
    message_code = 'message_bundle.action.waiting'
    message_text = get_message_text(message)
    if is_operator(user.chat_id):
        message_code += '.operator'
        match = re.search(OPERATOR_ANSWER_REGEX, message_text)
        if match and User.find_by_chat_id(match[1]):
            message_code += '.corrected'
            telegram.Bot(get_env_var('BOT_TOKEN')).send_message(match[1], match[2])
        else:
            message.reply_text(SENTENCE_DETERMINANT.get_closest_question(message_text))

        #     message_code += '.uncorrected'
        # message.reply_text(get_text(
        #     message_code,
        #     User.find_by_chat_id(message.chat.id).language
        # ))
    else:
        message.reply_text(SENTENCE_DETERMINANT.get_closest_question(message_text))


