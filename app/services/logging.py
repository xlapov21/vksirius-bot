from telegram import Update, Message
from datetime import datetime


def log(update: Update):
    file_log = open('message_log.txt', 'a')
    message = update.message
    user = message.from_user
    file_log.write(
        f'{str(datetime.now())}.  ChatID: {message.chat.id}. UserName: {user.username}: {user.last_name} {user.first_name} - {get_message_text(message)}\n')


def get_message_text(message: Message):
    if message.text:
        text = message.text
    elif message.caption:
        text = message.caption
    else:
        text = '[no text]'
    return text
