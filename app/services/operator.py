import random
from lib.env_var_selector import get_env_var


def get_random_operator():
    return random.choice(get_operators_list())


def get_operators_list():
    return get_env_var('OPERATORS_CHATS').split(', ')


def is_operator(chat_id):
    return chat_id in get_operators_list()
