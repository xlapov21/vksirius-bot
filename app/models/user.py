from lib.db_connector import DBConnector


class User:
    def __init__(self, first_name, last_name, user_name, chat_id, current_action, operator_id, language):
        self.first_name = first_name
        self.last_name = last_name
        self.user_name = user_name
        self.chat_id = chat_id
        self.current_action = current_action
        self.current_operator_id = operator_id
        self.language = language

    @staticmethod
    def all():
        users = []
        results = DBConnector.execute("""SELECT * FROM users""")
        for user in results:
            users.append(User(user[1], user[2], user[3], user[4], user[5], user[6], user[7]))
        return users

    @staticmethod
    def find_by_chat_id(chat_id):
        result = DBConnector.execute("""SELECT * FROM users WHERE chat_id = %(chat_id)s""", {'chat_id': str(chat_id)})
        user = result.fetchone()
        if user:
            return User(user[1], user[2], user[3], user[4], user[5], user[6], user[7])
        else:
            return None

    def save(self):
        DBConnector.execute_commit(
            """UPDATE users SET 
                first_name = %(first_name)s,
                last_name = %(last_name)s,
                user_name = %(user_name)s,
                current_action = %(current_action)s,
                current_operator_id = %(current_operator_id)s, 
                language = %(language)s
                WHERE chat_id = %(chat_id)s""",
            {'first_name': self.first_name,
             'last_name': self.last_name,
             'user_name': self.user_name,
             'current_action': self.current_action,
             'current_operator_id': self.current_operator_id,
             'language': self.language,
             'chat_id': self.chat_id})

    @staticmethod
    def create(chat_id, user_name, last_name, first_name, current_operator_id):
        DBConnector.execute_commit(
            """INSERT INTO users  ( first_name,
                                    last_name, 
                                    user_name,
                                    chat_id, 
                                    current_action, 
                                    current_operator_id, 
                                    language)
                            VALUES( %(first_name)s, 
                                    %(last_name)s, 
                                    %(user_name)s,
                                    %(chat_id)s,
                                    %(current_action)s,
                                    %(current_operator_id)s,
                                    %(language)s)""",
            {'first_name': first_name,
             'last_name': last_name,
             'user_name': user_name,
             'current_action': 'waiting',
             'current_operator_id': current_operator_id,
             'language': 'ru',
             'chat_id': chat_id})
        return User.find_by_chat_id(chat_id)

    def destroy(self):
        DBConnector.execute_commit("""DELETE FROM users WHERE chat_id = %(chat_id)s""", {'chat_id': self.chat_id})
