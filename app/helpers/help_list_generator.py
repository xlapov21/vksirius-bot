from lib.message_selector import get_text

COMMAND_LIST = [
    'start',
    'set_language',
    'request_operator',
    'FAQ',
    'change_operator',
    'help'
]


def generate_text_list(language):
    text_list = ''
    for command in COMMAND_LIST:
        text_list += f"/{command} - {get_text('message_bundle.command.' + command + '.description', language)}\n"
    return text_list
