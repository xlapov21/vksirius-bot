from app.models.faq import FAQ


def generate_text_list(language):
    text_list = ''
    if language == 'ru':
        for question in FAQ.all():
            text_list += str(question.id) + ". " + question.title_ru + '\n'
    elif language == 'en':
        for question in FAQ.all():
            text_list += str(question.id) + ". " + question.title_en + '\n'
    return text_list
