CREATE TABLE IF NOT EXISTS languages
(
    id    integer GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    code  text UNIQUE NOT NULL,
    title text NOT NULL
);