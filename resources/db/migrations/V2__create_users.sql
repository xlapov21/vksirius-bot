CREATE TABLE IF NOT EXISTS users
(
    id                  integer GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    first_name          text,
    last_name           text,
    user_name           text UNIQUE,
    chat_id             text UNIQUE NOT NULL,
    current_action      text        NOT NULL,
    current_operator_id text        NOT NULL,
    language            text REFERENCES languages (code)
);