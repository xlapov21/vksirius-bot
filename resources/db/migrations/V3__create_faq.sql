CREATE TABLE IF NOT EXISTS faq
(
    id                  integer GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    title_ru            text UNIQUE NOT NULL,
    title_en            text UNIQUE NOT NULL,
    answer_ru           text        NOT NULL,
    answer_en text      text        NOT NULL,
);
