import i18n


# print(get_text('message_bundle.command.start', 'ru'))
def get_text(address, locale):
    i18n.load_path.append('resources/locales')
    return i18n.t(address, locale=locale)
