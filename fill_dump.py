from lib.db_connector import DBConnector

DBConnector.execute("""CREATE TABLE IF NOT EXISTS languages
(
    id    integer GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    code  text UNIQUE NOT NULL,
    title text NOT NULL
);

CREATE TABLE IF NOT EXISTS users
(
    id                  integer GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    first_name          text,
    last_name           text,
    user_name           text UNIQUE,
    chat_id             text UNIQUE NOT NULL,
    current_action      text        NOT NULL,
    current_operator_id text        NOT NULL,
    language            text REFERENCES languages (code)
);

CREATE TABLE IF NOT EXISTS faq
(
    id                  integer GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    title_ru            text UNIQUE NOT NULL,
    title_en            text UNIQUE NOT NULL,
    answer_ru           text        NOT NULL,
    answer_en           text        NOT NULL
);

INSERT INTO languages (code, title)
VALUES ('ru', 'Русский'),
       ('en', 'English')
;

INSERT INTO faq (title_ru, title_en, answer_ru, answer_en)
VALUES ('Что такое программирование?', 'What is programming?',
        'Программирование – это процесс создания компьютерной программы, включающий в себя проектирование программы, использование алгоритмов, написание кода программы и так далее.',
        'Programming is the process of creating a computer program, which includes designing a program, using algorithms, writing program code, and so on.'),
        ('Как работает программный код?', 'How does the program code work?',
        'Код программы содержит инструкции, которые являются исполняемыми командами, выполняемыми машиной после преобразования кода компилятором.',
        'The program code contains instructions, which are executable commands executed by the machine after the code is converted by the compiler.'),
        ('Что такое отладка?', 'What is debugging??',
        'Отладка – это процесс поиска и устранения ошибок в программе.',
        'Debugging is the process of finding and fixing errors in a program.'),
        ('Назовите типы ошибок, которые могут возникнуть в программе', 'Name the types of errors that may occur in the program',
        '1. Синтаксические ошибки. 2. Runtime errors или ошибки времени выполнения. 3. Логические ошибки.',
        '1. Syntax errors. 2. Runtime errors. 3. Logical errors.'),
        ('Расскажите о синтаксических ошибках', 'Tell us about syntax errors',
        'Синтаксическая ошибка возникает, когда в коде программы допущена опечатка или есть несоответствие правилам используемого языка в языковой конструкции, имени переменной, функции и так далее. Ошибка определяется на этапе компиляции программы.',
        'A syntax error occurs when a typo is made in the program code or there is a discrepancy between the rules of the language used in the language construction, variable name, function, and so on. The error is detected at the compilation stage of the program.'),
        ('Расскажите об ошибке времени выполнения', 'Tell us about the runtime error',
        'Runtime error возникает в случае, когда программа пытается совершить нелегитимное действие, например, поделить на ноль. Ошибка времени выполнения может появиться на любом этапе работы программы, когда совершается неправильное действие. В случае возникновения этой ошибки машина останавливает выполнение программы и может показать диагностическое сообщение, объясняющее суть ошибки.',
        'Runtime error occurs when a program tries to perform an illegitimate action, for example, divide by zero. A runtime error can appear at any stage of the program when an incorrect action is performed. If this error occurs, the machine stops the program execution and can show a diagnostic message explaining the essence of the error.'),
        ('Расскажите о логических ошибках', 'Tell us about logical errors',
        'Логическую ошибку труднее всего определить, так как она может не проявляться при компиляции и во время выполнения программы, но при этом приводит к ее неправильному выполнению. Логическая ошибка может возникнуть как из-за неправильного применения алгоритма, так и из-за простого чтения/записи неправильной переменной.',
        'The logical error is the most difficult to determine, since it may not manifest itself during compilation and during program execution, but at the same time leads to its incorrect execution. A logical error can occur either due to incorrect application of the algorithm, or due to a simple reading/writing of the wrong variable.'),
        ('Что такое блок-схема?', 'What is a flowchart?',
        'Блок-схема – это графическое представление программы. Блок-схема помогает понять логику работы программы или ее части при проектировании.',
        'A flowchart is a graphical representation of a program. The flowchart helps to understand the logic of the program or part of it when designing.'),
        ('Что такое алгоритм?', 'What is an algorithm?',
        'Алгоритм – это конечный набор шагов, которые при следовании им решают какую-то задачу.',
        'An algorithm is a finite set of steps that, when followed, solve a problem.'),
        ('Что такое переменные?', 'What are the variables?',
        'Переменные – это именованные ячейки памяти, которые используются для хранения данных программы, результатов ее вычислений. Значение переменной может меняться в ходе выполнения программы.',
        'Variables are named memory cells that are used to store program data and the results of its calculations. The value of the variable may change during the execution of the program.'),
        ('Что такое цикл?', 'What is a loop?',
        'Цикл – это языковая конструкция, которая может определять участок программы для многократного повторения и количество этих повторений.',
        'A loop is a language construct that can define a section of a program for repeated repetition and the number of these repetitions.'),
        ('Что такое документация?', 'What is documentation?',
        'Документация – это подробное описание алгоритмов и частей программы, процесса проектирования, тестирования и паттернов правильного использования. Документация может предназначаться как для пользователя программы, так и для разработчика.',
        'Documentation is a detailed description of the algorithms and parts of the program, the design process, testing and patterns of proper use. The documentation can be intended for both the user of the program and the developer.'),
        ('Что делает компилятор?', 'What does the compiler do?',
        'Компилятор «читает» код, написанный на определенном языке программирования, и преобразует описанные команды и конструкции языка в исполняемый машинный код.',
        'The compiler "reads" code written in a specific programming language and converts the described commands and language constructs into executable machine code.'),
        ('Что такое двоичный код?', 'What is binary code?',
        'Двоичный код – это бинарная форма представления кода определенного языка программирования.',
        'Binary code is a binary form of representation of the code of a particular programming language.'),
        ('Что такое массив?', 'What is an array?',
        'Массив – это набор смежных областей памяти, которые хранят данные определенного типа.',
        'An array is a set of contiguous memory areas that store data of a certain type.'),
        ('Что такое машинный код?', 'What is machine code?',
        'Машинный код – это язык программирования, который может обрабатываться напрямую процессором, без необходимости предварительной компиляции.',
        'Machine code is a programming language that can be processed directly by the processor, without the need for pre–compilation.');
""")
